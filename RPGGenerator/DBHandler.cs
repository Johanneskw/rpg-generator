﻿using ClassStructureRPG;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Windows.Forms;

namespace RPGGenerator
{
    static class DBHandler
    {
        static string connectionString = @"Server=PC7385\SQLEXPRESS;Database=Characters;Trusted_Connection=True;";

        //Insert method, first inserts character data to character schema, then does a select to get ID of the created character, to be used as foreign key in the type-dependant schema. 
        public static void Insert(Character ch)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO Character (Name, CharacterType, HP, Stamina, ArmorRating, CharacterLevel, Strength) VALUES (@Name, @Type, @HP, @Stamina, @ArmRate, @CharLvl, @Strength)";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@Name", ch.Name);
                    command.Parameters.AddWithValue("@Type", ch.GetType().Name);
                    command.Parameters.AddWithValue("@HP", ch.HP);
                    command.Parameters.AddWithValue("@Stamina", ch.Stamina);
                    command.Parameters.AddWithValue("@ArmRate", ch.ArmorRating);
                    command.Parameters.AddWithValue("@CharLvl", ch.CharacterLVL);
                    command.Parameters.AddWithValue("@Strength", ch.Strength);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                //Getting the characterID to use as foreign key in the subclass schemas. 
                int characterID = -1;
                string sql = "SELECT TOP 1 * FROM Character ORDER BY CharacterID DESC";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            characterID = reader.GetInt32(0);
                        } 
                    }
                }

                string table ="";
                string column = "";
                int intVal = 0;
                string strVal = "";
                if (ch is Warrior w)
                {
                    table = "Warrior";
                    column = "WarriorBoost";
                    intVal = w.Boost;
                }
                if (ch is Rouge r)
                {
                    table = "Rouge";
                    column = "SpecialAttack";
                    strVal = r.SpecialAttack;
                }
                if (ch is Ranger ra)
                {
                    table = "Ranger";
                    column = "AttackRange";
                    intVal = ra.AttackRange;
                }
                if (ch is Wizard wi)
                {
                    table = "Wizard";
                    column = "MagicType";
                    strVal = wi.MagicType;
                }
                string subQuery = $"INSERT INTO {table} ({column}, CharacterID) VALUES (@Value, @CharID)";
                using (SqlCommand command = new SqlCommand(subQuery, connection))
                {
                    if (ch is Warrior || ch is Ranger) command.Parameters.AddWithValue("@Value", intVal);
                    else command.Parameters.AddWithValue("@Value", strVal);
                    command.Parameters.AddWithValue("@CharID", characterID);
                    command.ExecuteNonQuery();
                }
            }
        }
        //Gets data from all created characters, to be displayed in the GUI Listbox. 
        public static void SelectChars(ListBox lb)
        {
            List<string> characters = new List<string>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "SELECT * FROM Character ORDER BY CharacterID DESC";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            characters.Add(reader.GetInt32(0) + " " + reader.GetString(1).Trim() + " " + reader.GetString(2).Trim());
                        }
                    }
                }
            }
            lb.DataSource = characters;
        }
        //delete character from both schemas using characterID as key.
        public static void DeleteCharacter(int charID, string charType)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string schemaName = GetSchemaName(charType);
                string query = "DELETE FROM Character WHERE CharacterID = @CharID";
                string subQuery = $"DELETE FROM {schemaName} WHERE CharacterID = @charID";
                    
                connection.Open();
                using (SqlCommand command = new SqlCommand(subQuery, connection))
                {
                    command.Parameters.AddWithValue("@CharID", charID);
                    command.ExecuteNonQuery();
                }
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@CharID", charID);
                    command.ExecuteNonQuery();
                }
            }
        }

        //Intermediate storage to access the values from the generator form class...
        public static string name;
        public static string charType;
        public static int hp;
        public static int stamina;
        public static int armRate;
        public static int charlvl;
        public static int strength;
        public static int customI;
        public static string customS;
        //Selects the character picked from the listbox, and gets all the data, to be displayed in the input-fields of the GUI.
        public static void SelectCurrent(int charID, string CharT)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string dbType = GetSchemaName(CharT);
                string query = "SELECT * FROM Character WHERE CharacterID = @CharID";
                string subQuery = $"SELECT * FROM {dbType} WHERE CharacterID = @CharID";
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@CharID", charID);
                    command.ExecuteNonQuery();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            name = reader.GetString(1).Trim();
                            charType = reader.GetString(2);
                            hp = reader.GetInt32(3);
                            stamina = reader.GetInt32(4);
                            armRate = reader.GetInt32(5);
                            charlvl = reader.GetInt32(6);
                            strength = reader.GetInt32(7);
                        }
                    }
                }
                using (SqlCommand command = new SqlCommand(subQuery, connection))
                {
                    command.Parameters.AddWithValue("@CharID", charID);
                    command.ExecuteNonQuery();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.GetFieldType(1) == typeof(string)) customS = reader.GetString(1).Trim();
                            else customI = reader.GetInt32(1);
                        }
                    }
                }
            }
        }
        //Updates all information of a character, using input collected. if values has not been changed, the values will stay the same.
        public static void UpdateRow(int charID, string charT,  Character ch)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE Character SET Name = @name, HP = @hp, ArmorRating = @armrate, CharacterLevel = @charlvl, Strength = @strength WHERE CharacterID = @CharID";
                string colname = "";
                string strVal = "";
                string schemaName = GetSchemaName(charT);
                int intVal = -1;
                if (ch is Warrior w)
                {
                    colname = "WarriorBoost";
                    intVal = w.Boost;
                }
                if (ch is Rouge r)
                {
                    colname = "SpecialAttack";
                    strVal = r.SpecialAttack;
                }
                if (ch is Ranger ra)
                {

                    colname = "AttackRange";
                    intVal = ra.AttackRange;
                }
                if (ch is Wizard wi)
                {
                    colname = "MagicType";
                    strVal = wi.MagicType;
                }
                string subQuery = $"UPDATE {schemaName} SET {colname} = @Value WHERE CharacterID = @CharID";
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@name", ch.Name);
                    command.Parameters.AddWithValue("@hp", ch.HP);
                    command.Parameters.AddWithValue("@stamina", ch.Stamina);
                    command.Parameters.AddWithValue("@armrate", ch.ArmorRating);
                    command.Parameters.AddWithValue("@charlvl", ch.CharacterLVL);
                    command.Parameters.AddWithValue("@strength", ch.Strength);
                    command.Parameters.AddWithValue("@charID", charID);
                    command.ExecuteNonQuery();
                }
                using (SqlCommand command = new SqlCommand(subQuery, connection))
                {
                    if (ch is Warrior || ch is Ranger) command.Parameters.AddWithValue("@Value", intVal);
                    else command.Parameters.AddWithValue("@Value", strVal);
                    command.Parameters.AddWithValue("@charID", charID);
                    command.ExecuteNonQuery();
                }
            }
        }

        //Gets the correct sub-scema of the different sub types of a character. 
        public static string GetSchemaName(string cType)
        {
            string dbType = cType;
            if (cType == "Necromancer") dbType = "Wizard";
            if (cType == "Ninja") dbType = "Rouge";
            if (cType == "Scout" || cType == "Archer") dbType = "Ranger";
            return dbType;
        }
    }
}
