﻿namespace RPGGenerator
{
    partial class Generator
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblChoseChar = new System.Windows.Forms.Label();
            this.CBCharPick = new System.Windows.Forms.ComboBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblCharName = new System.Windows.Forms.Label();
            this.tbCharName = new System.Windows.Forms.TextBox();
            this.lblHP = new System.Windows.Forms.Label();
            this.lblStamina = new System.Windows.Forms.Label();
            this.lblStrength = new System.Windows.Forms.Label();
            this.lblArmorRating = new System.Windows.Forms.Label();
            this.lblCharLvl = new System.Windows.Forms.Label();
            this.tbCustom = new System.Windows.Forms.TextBox();
            this.nudHP = new System.Windows.Forms.NumericUpDown();
            this.nudStamina = new System.Windows.Forms.NumericUpDown();
            this.nudStrength = new System.Windows.Forms.NumericUpDown();
            this.nudArmRate = new System.Windows.Forms.NumericUpDown();
            this.nudCharLvl = new System.Windows.Forms.NumericUpDown();
            this.nudCustom = new System.Windows.Forms.NumericUpDown();
            this.lblCustom = new System.Windows.Forms.Label();
            this.lblOutput = new System.Windows.Forms.Label();
            this.lbDisplayChars = new System.Windows.Forms.ListBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblCreated = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStamina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStrength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudArmRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCharLvl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCustom)).BeginInit();
            this.SuspendLayout();
            // 
            // LblChoseChar
            // 
            this.LblChoseChar.AutoSize = true;
            this.LblChoseChar.Location = new System.Drawing.Point(564, 13);
            this.LblChoseChar.Name = "LblChoseChar";
            this.LblChoseChar.Size = new System.Drawing.Size(159, 20);
            this.LblChoseChar.TabIndex = 1;
            this.LblChoseChar.Text = "Choose character type:";
            // 
            // CBCharPick
            // 
            this.CBCharPick.FormattingEnabled = true;
            this.CBCharPick.Location = new System.Drawing.Point(564, 35);
            this.CBCharPick.Name = "CBCharPick";
            this.CBCharPick.Size = new System.Drawing.Size(159, 28);
            this.CBCharPick.TabIndex = 2;
            this.CBCharPick.SelectedIndexChanged += new System.EventHandler(this.CBCharPick_SelectedIndexChanged);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(565, 383);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(158, 32);
            this.btnCreate.TabIndex = 3;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblCharName
            // 
            this.lblCharName.AutoSize = true;
            this.lblCharName.Location = new System.Drawing.Point(13, 13);
            this.lblCharName.Name = "lblCharName";
            this.lblCharName.Size = new System.Drawing.Size(116, 20);
            this.lblCharName.TabIndex = 4;
            this.lblCharName.Text = "Character Name";
            // 
            // tbCharName
            // 
            this.tbCharName.Location = new System.Drawing.Point(13, 36);
            this.tbCharName.Name = "tbCharName";
            this.tbCharName.Size = new System.Drawing.Size(125, 27);
            this.tbCharName.TabIndex = 5;
            // 
            // lblHP
            // 
            this.lblHP.AutoSize = true;
            this.lblHP.Location = new System.Drawing.Point(13, 70);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(28, 20);
            this.lblHP.TabIndex = 6;
            this.lblHP.Text = "HP";
            // 
            // lblStamina
            // 
            this.lblStamina.AutoSize = true;
            this.lblStamina.Location = new System.Drawing.Point(13, 127);
            this.lblStamina.Name = "lblStamina";
            this.lblStamina.Size = new System.Drawing.Size(63, 20);
            this.lblStamina.TabIndex = 8;
            this.lblStamina.Text = "Stamina";
            // 
            // lblStrength
            // 
            this.lblStrength.AutoSize = true;
            this.lblStrength.Location = new System.Drawing.Point(13, 185);
            this.lblStrength.Name = "lblStrength";
            this.lblStrength.Size = new System.Drawing.Size(65, 20);
            this.lblStrength.TabIndex = 10;
            this.lblStrength.Text = "Strength";
            // 
            // lblArmorRating
            // 
            this.lblArmorRating.AutoSize = true;
            this.lblArmorRating.Location = new System.Drawing.Point(13, 243);
            this.lblArmorRating.Name = "lblArmorRating";
            this.lblArmorRating.Size = new System.Drawing.Size(94, 20);
            this.lblArmorRating.TabIndex = 12;
            this.lblArmorRating.Text = "Armor rating";
            // 
            // lblCharLvl
            // 
            this.lblCharLvl.AutoSize = true;
            this.lblCharLvl.Location = new System.Drawing.Point(13, 301);
            this.lblCharLvl.Name = "lblCharLvl";
            this.lblCharLvl.Size = new System.Drawing.Size(107, 20);
            this.lblCharLvl.TabIndex = 14;
            this.lblCharLvl.Text = "Character level";
            // 
            // tbCustom
            // 
            this.tbCustom.Location = new System.Drawing.Point(13, 383);
            this.tbCustom.Name = "tbCustom";
            this.tbCustom.Size = new System.Drawing.Size(125, 27);
            this.tbCustom.TabIndex = 17;
            this.tbCustom.Visible = false;
            // 
            // nudHP
            // 
            this.nudHP.Location = new System.Drawing.Point(13, 94);
            this.nudHP.Name = "nudHP";
            this.nudHP.Size = new System.Drawing.Size(125, 27);
            this.nudHP.TabIndex = 21;
            // 
            // nudStamina
            // 
            this.nudStamina.Location = new System.Drawing.Point(12, 150);
            this.nudStamina.Name = "nudStamina";
            this.nudStamina.Size = new System.Drawing.Size(126, 27);
            this.nudStamina.TabIndex = 22;
            // 
            // nudStrength
            // 
            this.nudStrength.Location = new System.Drawing.Point(12, 209);
            this.nudStrength.Name = "nudStrength";
            this.nudStrength.Size = new System.Drawing.Size(126, 27);
            this.nudStrength.TabIndex = 23;
            // 
            // nudArmRate
            // 
            this.nudArmRate.Location = new System.Drawing.Point(13, 267);
            this.nudArmRate.Name = "nudArmRate";
            this.nudArmRate.Size = new System.Drawing.Size(125, 27);
            this.nudArmRate.TabIndex = 24;
            // 
            // nudCharLvl
            // 
            this.nudCharLvl.Location = new System.Drawing.Point(12, 325);
            this.nudCharLvl.Name = "nudCharLvl";
            this.nudCharLvl.Size = new System.Drawing.Size(126, 27);
            this.nudCharLvl.TabIndex = 25;
            // 
            // nudCustom
            // 
            this.nudCustom.Location = new System.Drawing.Point(13, 383);
            this.nudCustom.Name = "nudCustom";
            this.nudCustom.Size = new System.Drawing.Size(125, 27);
            this.nudCustom.TabIndex = 26;
            this.nudCustom.Visible = false;
            // 
            // lblCustom
            // 
            this.lblCustom.AutoSize = true;
            this.lblCustom.Location = new System.Drawing.Point(12, 359);
            this.lblCustom.Name = "lblCustom";
            this.lblCustom.Size = new System.Drawing.Size(56, 20);
            this.lblCustom.TabIndex = 27;
            this.lblCustom.Text = "default";
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(235, 94);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(50, 20);
            this.lblOutput.TabIndex = 28;
            this.lblOutput.Text = "label1";
            this.lblOutput.Visible = false;
            // 
            // lbDisplayChars
            // 
            this.lbDisplayChars.FormattingEnabled = true;
            this.lbDisplayChars.ItemHeight = 20;
            this.lbDisplayChars.Location = new System.Drawing.Point(565, 104);
            this.lbDisplayChars.Name = "lbDisplayChars";
            this.lbDisplayChars.Size = new System.Drawing.Size(158, 264);
            this.lbDisplayChars.TabIndex = 29;
            this.lbDisplayChars.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(235, 383);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(159, 32);
            this.btnDelete.TabIndex = 30;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblCreated
            // 
            this.lblCreated.AutoSize = true;
            this.lblCreated.Location = new System.Drawing.Point(565, 70);
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Size = new System.Drawing.Size(134, 20);
            this.lblCreated.TabIndex = 31;
            this.lblCreated.Text = "Created Characters";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(400, 383);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(159, 32);
            this.btnUpdate.TabIndex = 32;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblCreated);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lbDisplayChars);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.lblCustom);
            this.Controls.Add(this.nudCustom);
            this.Controls.Add(this.nudCharLvl);
            this.Controls.Add(this.nudArmRate);
            this.Controls.Add(this.nudStrength);
            this.Controls.Add(this.nudStamina);
            this.Controls.Add(this.nudHP);
            this.Controls.Add(this.tbCustom);
            this.Controls.Add(this.lblCharLvl);
            this.Controls.Add(this.lblArmorRating);
            this.Controls.Add(this.lblStrength);
            this.Controls.Add(this.lblStamina);
            this.Controls.Add(this.lblHP);
            this.Controls.Add(this.tbCharName);
            this.Controls.Add(this.lblCharName);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.CBCharPick);
            this.Controls.Add(this.LblChoseChar);
            this.Name = "Generator";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Generator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStamina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStrength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudArmRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCharLvl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCustom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LblChoseChar;
        private System.Windows.Forms.ComboBox CBCharPick;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblCharName;
        private System.Windows.Forms.TextBox tbCharName;
        private System.Windows.Forms.Label lblHP;
        private System.Windows.Forms.Label lblStamina;
        private System.Windows.Forms.Label lblStrength;
        private System.Windows.Forms.Label lblArmorStrengt;
        private System.Windows.Forms.Label lblArmorRating;
        private System.Windows.Forms.Label lblCharLvl;
        private System.Windows.Forms.TextBox tbCustom;
        private System.Windows.Forms.NumericUpDown nudHP;
        private System.Windows.Forms.NumericUpDown nudStamina;
        private System.Windows.Forms.NumericUpDown nudStrength;
        private System.Windows.Forms.NumericUpDown nudArmRate;
        private System.Windows.Forms.NumericUpDown nudCharLvl;
        private System.Windows.Forms.NumericUpDown nudCustom;
        private System.Windows.Forms.Label lblCustom;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.ListBox lbDisplayChars;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblCreated;
        private System.Windows.Forms.Button btnUpdate;
    }
}

