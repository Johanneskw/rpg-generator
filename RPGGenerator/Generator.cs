﻿using ClassStructureRPG;
using System;
using System.Collections.Generic;
using CsvHelper;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace RPGGenerator
{
    public partial class Generator : Form
    {
        public Generator()
        {
            InitializeComponent();
            DBHandler.SelectChars(lbDisplayChars);
        }

        //Global decleration of dictionary for referencing in other functions.
        Dictionary<int, string> chars = new Dictionary<int, string>();
        private void Generator_Load(object sender, EventArgs e)
        {
            chars.Add(0, "Warrior");
            chars.Add(1, "Rouge");
            chars.Add(2, "Ninja");
            chars.Add(3, "Wizard");
            chars.Add(4, "Necromancer");
            chars.Add(5, "Archer");
            chars.Add(6, "Scout");

            CBCharPick.ValueMember = "Key";
            CBCharPick.DisplayMember = "Value";
            CBCharPick.DataSource = new BindingSource(chars, null);

            lblOutput.Text = "1. Coose character type and fill \ninn properties to create new Character.\n\n2. Choose from created characters \nto update or delete a character.";
            lblOutput.Visible = true;
        }

        private void CBCharPick_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Ugly switch, which customizes the last field of the GuI depending on character rype. 
            resetFields();
            switch (CBCharPick.SelectedIndex)
            {
                case 0:
                    lblCustom.Text = "Warrior strength boost";
                    nudCustom.Visible = true;
                    tbCustom.Visible = false;
                    break;
                case 1:
                    lblCustom.Text = "Special attack";
                    nudCustom.Visible = false;
                    tbCustom.Visible = true;
                    break;
                case 2:
                    lblCustom.Text = "Special attack";
                    nudCustom.Visible = false;
                    tbCustom.Visible = true;
                    break;
                case 3:
                    lblCustom.Text = "Magic type";
                    nudCustom.Visible = false;
                    tbCustom.Visible = true;
                    break;
                case 4:
                    lblCustom.Text = "Magic type";
                    nudCustom.Visible = false;
                    tbCustom.Visible = true;
                    break;
                case 5:
                    lblCustom.Text = "Range of Attack";
                    nudCustom.Visible = true;
                    tbCustom.Visible = false;
                    break;
                case 6:
                    lblCustom.Text = "Range of Attack";
                    nudCustom.Visible = true;
                    tbCustom.Visible = false;
                    break;
            }
        }

        //Collects input data from GUI, and creates and returns a character object based on user-pick.   
        private Character collectNCreate()
        {
            string name = tbCharName.Text.Trim();
            int hp = Convert.ToInt32(nudHP.Value); 
            int stamina = Convert.ToInt32(nudStamina.Value);
            int strength = Convert.ToInt32(nudStrength.Value);
            int charlvl = Convert.ToInt32(nudCharLvl.Value);
            int armRating = Convert.ToInt32(nudArmRate.Value);
            int custm = Convert.ToInt32(nudCustom.Value);
            string scust = tbCustom.Text.Trim();
            string outp = "";
            Character character = null;
            switch (CBCharPick.SelectedIndex)
            {
                case 0:
                    character = new Warrior(name, hp, stamina, armRating, charlvl, strength, custm);
                    break;
                case 1:
                    character = new Rouge(name, hp, stamina, armRating, charlvl, strength, scust);
                    break;
                case 2:
                    character = new Ninja(name, hp, stamina, armRating, charlvl, strength, scust);
                    break;
                case 3:
                    character = new Wizard(name, hp, stamina, armRating, charlvl, strength, scust);
                    break;
                case 4:
                    character= new Necromancer(name, hp, stamina, armRating, charlvl, strength, scust);
                    break;
                case 5:
                    character = new Archer(name, hp, stamina, armRating, charlvl, strength, custm);
                    break;
                case 6:
                    character = new Scout(name, hp, stamina, armRating, charlvl, strength, custm);
                    break;
            }
            return character;
        }

        //Generates a string with character information to dipslay to the user. 
        private string generateString(Character ch)
        {
            string outp = $"Character created\n{ch.Name}\nLevel: {ch.CharacterLVL} \nHP: {ch.HP}\nStrength: {ch.Strength}\nArmor rating: {ch.ArmorRating}";
            if (ch is Warrior war) outp += $"\nStrengt bonus: {war.Boost}\n{war.Walk()}\n{war.Attack()}";
            else if (ch is Ninja nin) outp += $"\nSpecial attack: {nin.SpecialAttack}\n{nin.Run()}\n{nin.Attack()}";
            else if (ch is Rouge rou) outp += $"\nSpecial attack: {rou.SpecialAttack}\n{rou.Run()}\n{rou.Attack()}";
            else if (ch is Necromancer nec) outp += $"\nMagic type: {nec.MagicType}\n{nec.Fly()}\n{nec.Attack()}";
            else if (ch is Wizard wiz) outp += $"\nMagic type: {wiz.MagicType}\n{wiz.Fly()}\n{wiz.Attack()}";
            else if (ch is Archer arc) outp += $"\nAttack range: {arc.AttackRange}\n{arc.Run()}\n{arc.Attack()}";
            else if (ch is Scout sco) outp += $"\nAttack range: {sco.AttackRange}\n{sco.Run()}\n{sco.Attack()}";
            return outp;
        }
        //Gets an character-object from collectNcreate, calls the insert method from dbHandler and inserts object to DB, displays messagebox with info, and refreshes the created characters listbox.
        private void btnCreate_Click(object sender, EventArgs e)
        {
            createCsv();
            Character ch = collectNCreate();
            DBHandler.Insert(ch);
            MessageBox.Show(generateString(ch));
            DBHandler.SelectChars(lbDisplayChars);
            resetFields(); 
        }
        //Creates an csv file for output. 
        public void createCsv()
        {
            try
            {
                using (StreamWriter sw = new StreamWriter("Characters.csv"))
                {
                    using (CsvWriter csv = new CsvWriter(sw, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        csv.WriteHeader<Character>();
                        csv.WriteField("MagicType");
                        csv.WriteField("Boost");
                        csv.WriteField("AttackRange");
                        csv.WriteField("SpecialAttack");
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        //Writes to Csv. TODO, fix overwrite of headers and old data...
        private void submitToCsv(Character ch)
        {
            using (StreamWriter sw = new StreamWriter("Characters.csv"))
            {
                //sw.WriteLine();
                using (CsvWriter csv = new CsvWriter(sw, System.Globalization.CultureInfo.CurrentCulture))
                {
                    csv.NextRecord();
                    if (ch is Warrior w)
                    {
                        csv.WriteRecord<Warrior>(w);
                    }
                    if (ch is Wizard wi)
                    {
                        csv.WriteRecord<Wizard>(wi);
                    }
                    if (ch is Rouge r)
                    {
                        csv.WriteRecord<Rouge>(r);
                    }
                    if (ch is Ranger ra)
                    {
                        csv.WriteRecord<Ranger>(ra);
                    }
                }
            }
        }
        //Cals a select method from dbHandler, gets the properties of chosen caracter, and dislays the data in input fields.
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lbText = lbDisplayChars.GetItemText(lbDisplayChars.SelectedItem);
            string[] bits = lbText.Split(" ");
            DBHandler.SelectCurrent(Int32.Parse(bits[0]), bits[bits.Length - 1]);
            CBCharPick.SelectedIndex = CBCharPick.FindStringExact(bits[bits.Length - 1]);
            setValues(bits[bits.Length -1]);

        }

        //Gets the index and type of character from the listbox, then calls delete-method(dbHandler) which deletes object from both schemas.
        private void btnDelete_Click(object sender, EventArgs e)
        {
            string lbText = lbDisplayChars.GetItemText(lbDisplayChars.SelectedItem);
            string[] bits = lbText.Split(" ");
            DBHandler.DeleteCharacter(Int32.Parse(bits[0]), bits[bits.Length-1]);
            DBHandler.SelectChars(lbDisplayChars);
        }

        //Gets values from intermediate storage in dbHandler, sets input field values.  
        private void setValues(string cType)
        {
            tbCharName.Text = DBHandler.name;
            nudHP.Value = DBHandler.hp;
            nudStamina.Value = DBHandler.stamina;
            nudArmRate.Value = DBHandler.armRate;
            nudCharLvl.Value = DBHandler.charlvl;
            nudStrength.Value = DBHandler.strength;
            if (cType == "Warrior" || cType == "Ranger" || cType == "Scout") nudCustom.Value = DBHandler.customI;
            else tbCustom.Text = DBHandler.customS;
        }
        //Collects data, and creates new object, replaces the row in the DB schemas with new object to update all fields.
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string lbText = lbDisplayChars.GetItemText(lbDisplayChars.SelectedItem);
            string[] bits = lbText.Split(" ");
            Character ch = collectNCreate();
            DBHandler.UpdateRow(Int32.Parse(bits[0]), bits[bits.Length - 1], ch);
            MessageBox.Show($"{ch.Name} updated");
        }

        //Resets the input fields.
        private void resetFields()
        {
            tbCharName.Text = "";
            nudHP.Value = 0;
            nudStamina.Value = 0;
            nudArmRate.Value = 0;
            nudCharLvl.Value = 0;
            nudStrength.Value = 0;
            nudCustom.Value = 0;
            tbCustom.Text = "";
        }
    }
}
